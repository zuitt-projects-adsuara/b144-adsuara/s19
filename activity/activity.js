/*<!-- Activity:
1. In the S19 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
14. Create a git repository named S19.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code. "s19 Activity"
16. Add the link in Boodle. "Javascript ES6 Updates" -->*/

let num = 3;
const getCube = num ** 3; 

console.log(`the cube of : ${num} is ${getCube}`)


const address = [100, "HighStreet", "California"];
const [hNumber,streetADD,state] = address
console.log(`Im From ${hNumber} ${streetADD} ${state}! HBU??`) 

let animal = {
	name: "HellBear",
	weight:300,
	height:6
}

const { name, weight, height } = animal;
console.log(`A ${name} is a animal from hell with an average weight of ${weight}kg and height of ${height}ft`) 


const number = [100,100,100,500,500,500];          
number.forEach(function(number){
	console.log(`${number}`)
})
const reducer = (prevValue, curValue) => prevValue + curValue;
console.log(number.reduce(reducer));


class dog {
	constructor(age, name, breed){
		this.age = age;
		this.name = name;
		this.breed = breed;
	}
}

const myDog = new dog();

myDog.age = '25';
myDog.name = 'Masarus';
myDog.breed = 'High-Breed Askal';

console.log(myDog)

