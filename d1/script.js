//Javascript ES6 Updates

//1. EXPONENT OPERATOR
//Pre version
const firstNum = Math.pow(8, 2);
console.log(firstNum)

//ES6
const secondNum = 8 ** 2;
console.log(secondNum)

//2. TEMPLATE LITERALS
//allows us to write strings without using the concatenation operator (+)
//Greatly helps with code readability


let name = "John";
//Pre-Template Literal String
//Uses single quotes (' ')
let message = 'Hello' + name + '! Welcome to programming!';
console.log("Message without template literals: " + message)

//Strings Using Template Literal
//Uses backticks (``)
message = `Hello
 ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`)

//Multi-line Using Template Literals
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with solution of ${secondNum}.`

console.log(anotherMessage)

//Template literals allow us to write strings with embedded JS expressiong ( ${} )

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`)

//3. ARRAY DESTRUCTURING
//Allows us to unpack elements in arrays into distinct variables
//Allows us to name array elements with variables instead of using index numbers. Helps with code readability.
//Syntax: let/const [variableName, variableName] = array


const fullName = ["Juan", "Dela", "Cruz"];

//Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you`)

//Array Destructuring
const [firstName, middleName, lastName] = fullName

console.log(firstName)
console.log(middleName)
console.log(lastName)

//expressions are any valid unit of code tha resolves to a value
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you`)

//4. OBJECT DESTRUCTURING
//Allows us to unpack elements in objects into distinct variables

const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
};

//Pre-Object Destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`)

//Object Destructuring
//Shortens the syntax for accessing properties from objects
//Syntax: let/const {propertyName, propertyName} = object

const { givenName, maidenName, familyName } = person;

console.log(givenName)
console.log(maidenName)
console.log(familyName)

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`)

function getFullName({ givenName, maidenName, familyName }){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person)

//5. ARROW FUNCTIONS
//Normal Function
function printFullName(fName, mName, lName){
	console.log(fName + ' ' + mName + ' ' + lName)
}
//function declaration
//function name
//parameters = placeholder/ the name of an argument to be passed to the function
//statements = function body
//invoke/call a function

printFullName('John', 'D.', 'Smith')

//ES6 Arrow Function
//Compact alternative syntax to traditional functions
//Useful for code snippets where creating functions will not be reused in any other portion of the code
//as anonymous function
//Adheres to the 'DRY' principle (Don't Repeat Yourself) where there's no longer need to create a function and think of a name for functions that will only be used in certain snippets
const variableName = () => {
	console.log('Hello World')
}

const printFName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lName}`)
}

printFName('Jane', 'D.', 'Smith')

//Arrow Functions with loops
//Pre-Arrow function

const students = ['John', 'Jane', 'Joe'];

students.forEach(function(student){
	console.log(`${student} is a student`)
})

//Arrow Function
students.forEach((student) => console.log(`${student} is a student.`));

//6. (Arrow function) IMPLICIT RETURN STATEMENT
//There are instances when you can omit the 'return' statement. This works because even without the 'return' statement, javascript implicitly adds it for the result of the function
//Pre-Arrow Function
/*const add = (x, y) => {
	return x + y;
}

let total = add(1, 2);
console.log(total)*/

//Arrow Function
const add = (x, y) => x + y

let total = add(1, 2);
console.log(total)

/*let filterFriends = friends.filter(friend => friend.length === 4)*/

//7. (Arrow Function) DEFAULT FUNCTION ARGUMENT VALUE
//Provides a default argument value if none is provided when the function is invoked

const greet = (name = 'User') => {
	return `Good morning, ${ name }`
}

console.log(greet())

//8. CREATING A CLASS
/*
Syntax:

	class className {
		constructor(objectPropertyA, objectPropertyB){
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectPropertyB
		}
	}

- The constructor is a special method of a class for creating/initializing an object for that class.
-The 'this' keyword refers to the properties of an object created from the class.
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar)

//Value of properties may be assigned after creation/instantiation of an object
myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;

console.log(myCar)

//Creating/instantiating a new object from the car class with initialized values 
const myNewCar = new Car('Toyota', 'Vios', 2021)

console.log(myNewCar)
